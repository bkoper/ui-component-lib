'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _button = require('./button');

var _button2 = _interopRequireDefault(_button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (props) {
    return _react2.default.createElement(
        'p',
        null,
        _react2.default.createElement(
            'b',
            null,
            'Label with button'
        ),
        _react2.default.createElement('br', null),
        _react2.default.createElement(
            _button2.default,
            null,
            'k'
        )
    );
};