'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _button = require('./button');

Object.defineProperty(exports, 'Button', {
  enumerable: true,
  get: function get() {
    return _button.Button;
  }
});

var _label = require('./label');

Object.defineProperty(exports, 'Label', {
  enumerable: true,
  get: function get() {
    return _label.Label;
  }
});