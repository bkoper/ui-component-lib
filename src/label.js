import React from 'react';
import Button from './button'

export default (props) => (
    <p>
        <b>Label with button</b>
        <br />
        <Button>k</Button>
    </p>
);
